This is a project to install oracle 10.2 on an Oracle Enterprise Linux 5.11 server.

The use of this software is at your own risk, and you need to fulfil the licensing requirements of the products used.
This software is legacy and out of support, this is created for demo and historic purposes.

This software is an aid to install oracle and create a database unattended.
Because this OS is so old, it can't run Ansible in a simple way, and therefore it's scripted in bash.
This means the scripts can not (nicely) be rerun (they are not idempotent). If it breaks, fix it, clean up the VM and try again.

Currently, it can install version 10.2.0.1, 10.2.0.2, 10.2.0.3, 10.2.0.4 and 10.2.0.5. 

## a) the environment
This project requires the following local installs:
- virtualbox
- vagrant
- git

## b) obtain the vagrant project and the scripts:
`git clone https://gitlab.com/FritsHoogland/ol511_oracle102.git o102`

## c) edit the o102/Vagrantfile
You have to enter a hostonly ip address with hostonly_network_ip_address.
By default, the database_name variable is empty. This means no database is created. If you want a database, set a name for one.
You also might want to change: database_characterset, redologfile_size, sga_target_mb, pga_aggregate_target_mb.
The database_version variable determines the version to be installed. The default version is 10.2.0.1, requiring only the cpio installation file. If you set it to 10.2.0.2, 10.2.0.3, 10.2.0.4 or 10.2.0.5, you must add the patch (zipfile) so it can be used.
Do not change anything else.

## d) add the software
In order to install the oracle database, you must add the oracle installation media to the o102/files directory:
- 10201_database_linux_x86_64.cpio
If you set another version than 10.2.0.1, you must add:
- 10.2.0.2: add: p4547817_10202_Linux-x86-64.zip
- 10.2.0.3: add: p5337014_10203_Linux-x86-64.zip
- 10.2.0.4: add: p6810189_10204_Linux-x86-64.zip
- 10.2.0.5: add: p8202632_10205_Linux-x86-64.zip

These patches need special access in the patch section in MOS.

## e) start vagrant
In order to get the linux image and get the oracle software installed and optionally get the database execute:

`vagrant up`

This will pull the FritsHoogland/oracle-5.11 box, start it, copy the files in the files directory into the vagrant box and do the installation.

## f) other noteworthy things
This is the 64 bit OS version and the install tested with the 64 bit oracle version.
This ancient oracle linux version fully works with vagrant and virtualbox.
The vagrant box FritsHoogland/oracle-5.11 is build using packer, here is the build project: https://gitlab.com/FritsHoogland/packer-oracle-linux-museum

If you see anything that might improve this, contact me at frits.hoogland_at_gmail.com
(replace _at_ with '@')