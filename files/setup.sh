#!/bin/bash
source ./install/settings.sh
./install/scripts/check_files.sh
[ $? -gt 0 ] && exit 1
./install/scripts/setup_u01.sh
./install/scripts/setup_oradata.sh
./install/scripts/install_oracle.sh
[ $database_version = "10.2.0.2" ] && ./install/scripts/patch_10202.sh
[ $database_version = "10.2.0.3" ] && ./install/scripts/patch_10203.sh
[ $database_version = "10.2.0.4" ] && ./install/scripts/patch_10204.sh
[ $database_version = "10.2.0.5" ] && ./install/scripts/patch_10205.sh
./install/scripts/create_database.sh
./install/scripts/setup_autostart.sh
