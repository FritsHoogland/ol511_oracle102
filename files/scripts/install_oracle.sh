#!/bin/bash
#
/bin/echo "copy files and install oracle database software"
#
/usr/bin/sudo /bin/mkdir -p $stage_directory/scripts
/usr/bin/sudo /bin/cp /home/vagrant/install/scripts/oracle/* $stage_directory/scripts
/usr/bin/sudo /bin/cp /home/vagrant/install/10201_database_linux_x86_64.cpio $stage_directory
/usr/bin/sudo /bin/chown -R oracle.oinstall $stage_directory
/bin/echo "export ORACLE_BASE=/u01/app/oracle
export ORACLE_HOME=/u01/app/oracle/product/10.2/dbhome_1
export PATH=/usr/sbin:\$ORACLE_HOME/bin:$PATH
export LD_LIBRARY_PATH=\$ORACLE_HOME/lib:/lib:/usr/lib
export CLASSPATH=\$ORACLE_HOME/JRE:\$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib" | sudo -u oracle /usr/bin/tee -a /home/oracle/.bash_profile > /dev/null
/usr/bin/sudo -u oracle sh -c "cd $stage_directory; /bin/cpio -idm < 10201_database_linux_x86_64.cpio"
/usr/bin/sudo -u oracle /bin/sed -i "s#__ORACLE_HOME__#/u01/app/oracle/product/10.2/dbhome_1#" $stage_directory/scripts/install.rsp
/usr/bin/sudo -u oracle /bin/sed -i "s/__DATABASE_NAME__/dummy/" $stage_directory/scripts/install.rsp
/usr/bin/sudo -u oracle /bin/sed -i "s/__GLOBAL_PASSWORD__/$global_password/" $stage_directory/scripts/install.rsp
/usr/bin/sudo -u oracle /bin/sed -i "s/__DATABASE_CHARSET__/$database_characterset/" $stage_directory/scripts/install.rsp
/usr/bin/sudo -u oracle $stage_directory/database/runInstaller -ignoresysprereqs -ignoreprereq -waitforcompletion -invPtrLoc /u01/install/scripts/oraInst.loc -silent -responsefile /u01/install/scripts/install.rsp
/usr/bin/sudo /u01/app/oracle/product/10.2/dbhome_1/root.sh -silent
