#!/bin/bash
#
/bin/echo "apply patch 10.2.0.4"
#
/usr/bin/sudo /bin/mkdir $stage_directory/patch_10204
/usr/bin/sudo /bin/cp /home/vagrant/install/p6810189_10204_Linux-x86-64.zip $stage_directory/patch_10204
/usr/bin/sudo /bin/chown -R oracle.oinstall $stage_directory
/usr/bin/sudo -u oracle sh -c "cd $stage_directory/patch_10204; /usr/bin/unzip p6810189_10204_Linux-x86-64.zip"
/usr/bin/sudo -u oracle $stage_directory/patch_10204/Disk1/runInstaller -silent -force -ignoresysprereqs -ignoreprereq -invPtrLoc /u01/install/scripts/oraInst.loc -waitforcompletion FROM_LOCATION="$stage_directory/patch_10204/Disk1/stage/products.xml" ORACLE_HOME="/u01/app/oracle/product/10.2/dbhome_1" ORACLE_HOME_NAME="OraDbHome1" TOPLEVEL_COMPONENT='{"oracle.patchset.db","10.2.0.4"}'
/usr/bin/sudo /u01/app/oracle/product/10.2/dbhome_1/root.sh -silent
