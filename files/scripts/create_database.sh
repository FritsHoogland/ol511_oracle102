#!/bin/bash
#
echo "update template and create database"
#
/usr/bin/sudo -u oracle /bin/sed -i "s/__SGA_TARGET__/$sga_target_mb/" $stage_directory/scripts/database.dbt
/usr/bin/sudo -u oracle /bin/sed -i "s/__PGA_AGGREGATE_TARGET__/$pga_aggregate_target_mb/" $stage_directory/scripts/database.dbt
/usr/bin/sudo -u oracle /bin/sed -i "s/__REDO_SIZE__/$redologfile_size/" $stage_directory/scripts/database.dbt
/usr/bin/sudo -u oracle /bin/sed -i "s/__DATABASE_VERSION__/$database_version/" $stage_directory/scripts/database.dbt
if [ ! -z "$database_name" ]; then
  /usr/bin/sudo -u oracle /bin/echo "/u01/app/oracle/product/10.2/dbhome_1/bin/dbca -silent -createdatabase -templatename /u01/install/scripts/database.dbt -gdbname $database_name -sid $database_name" | sudo -u oracle /usr/bin/tee $stage_directory/create_database.sh > /dev/null
  /usr/bin/sudo -u oracle /bin/chmod 755 $stage_directory/create_database.sh
  /usr/bin/sudo -u oracle -i $stage_directory/create_database.sh
fi
