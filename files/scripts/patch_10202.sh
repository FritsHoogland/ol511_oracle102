#!/bin/bash
#
/bin/echo "apply patch 10.2.0.2"
#
/usr/bin/sudo /bin/mkdir $stage_directory/patch_10202
/usr/bin/sudo /bin/cp /home/vagrant/install/p4547817_10202_Linux-x86-64.zip $stage_directory/patch_10202
/usr/bin/sudo /bin/chown -R oracle.oinstall $stage_directory
/usr/bin/sudo -u oracle sh -c "cd $stage_directory/patch_10202; /usr/bin/unzip p4547817_10202_Linux-x86-64.zip"
/usr/bin/sudo -u oracle $stage_directory/patch_10202/Disk1/runInstaller -silent -ignoresysprereqs -ignoreprereq -invPtrLoc /u01/install/scripts/oraInst.loc -force -waitforcompletion FROM_LOCATION="$stage_directory/patch_10202/Disk1/stage/products.xml" ORACLE_HOME="/u01/app/oracle/product/10.2/dbhome_1" ORACLE_HOME_NAME="OraDbHome1" TOPLEVEL_COMPONENT='{"oracle.patchset.db","10.2.0.2"}'
/usr/bin/sudo /u01/app/oracle/product/10.2/dbhome_1/root.sh -silent
