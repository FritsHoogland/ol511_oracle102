#!/bin/bash
#
/bin/echo "check for necessary files"
#
ERROR_COUNTER=0
#
if [ -f /home/vagrant/install/10201_database_linux_x86_64.cpio ]; then
  /bin/echo "OK - 10201_database_linux_x86_64.cpio"
else
  /bin/echo "FAIL - 10201_database_linux_x86_64.cpio"
  let ERROR_COUNTER++
fi

if [ $database_version = "10.2.0.2" ]; then
  if [ -f /home/vagrant/install/p4547817_10202_Linux-x86-64.zip ]; then
    /bin/echo "OK - p4547817_10202_Linux-x86-64.zip"
  else
    /bin/echo "FAIL - p4547817_10202_Linux-x86-64.zip"
    let ERROR_COUNTER++
  fi
fi

if [ $database_version = "10.2.0.3" ]; then
  if [ -f /home/vagrant/install/p5337014_10203_Linux-x86-64.zip ]; then
    /bin/echo "OK - p5337014_10203_Linux-x86-64.zip"
  else
    /bin/echo "FAIL - p5337014_10203_Linux-x86-64.zip"
    let ERROR_COUNTER++
  fi
fi

if [ $database_version = "10.2.0.4" ]; then
  if [ -f /home/vagrant/install/p6810189_10204_Linux-x86-64.zip ]; then
    /bin/echo "OK - p6810189_10204_Linux-x86-64.zip"
  else
    /bin/echo "FAIL - p6810189_10204_Linux-x86-64.zip"
    let ERROR_COUNTER++
  fi
fi

if [ $database_version = "10.2.0.5" ]; then
  if [ -f /home/vagrant/install/p8202632_10205_Linux-x86-64.zip ]; then
    /bin/echo "OK - p8202632_10205_Linux-x86-64.zip"
  else
    /bin/echo "FAIL - p8202632_10205_Linux-x86-64.zip"
    let ERROR_COUNTER++
  fi
fi

if [ $ERROR_COUNTER -gt 0 ]; then
  /bin/echo "No all files are present. Exiting"
  exit 1
fi
